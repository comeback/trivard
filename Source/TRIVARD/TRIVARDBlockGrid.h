// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Actor.h"
#include "TRIVARDBlock.h"
#include "TRIVARDBlockGrid.generated.h"


////Struct for storing and counting active blocks
USTRUCT()
struct FBlocksRow
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<ATRIVARDBlock*> Columns;

	void AddNewColumn()
	{
		Columns.Add(NULL);
	}
	//default properties
	FBlocksRow()
	{

	}
};

USTRUCT()
struct FBlocksGrid
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<FBlocksRow> Rows;

	void AddNewRow()
	{
		Rows.Add(FBlocksRow());
	}

	void AddUninitialized(const int32 RowCount, const int32 ColCount)
	{
		//Add Rows
		for (int32 v = 0; v < RowCount; v++)
		{
			AddNewRow();
		}

		//Add Columns
		for (int32 v = 0; v < RowCount; v++)
		{
			for (int32 v = 0; v < ColCount; v++)
			{
				Rows[v].AddNewColumn();
			}
		}
	}

	void Clear()
	{
		if (Rows.Num() <= 0) return;
		//~~~~~~~~~~~~~~~

		//Destroy any walls
		const int32 RowTotal = Rows.Num();
		const int32 ColTotal = Rows[0].Columns.Num();

		for (int32 v = 0; v < RowTotal; v++)
		{
			for (int32 b = 0; b < ColTotal; b++)
			{
				if (Rows[v].Columns[b] && Rows[v].Columns[b]->IsValidLowLevel())
				{
					Rows[v].Columns[b]->Destroy();
				}
			}
		}

		//Empty
		for (int32 v = 0; v < Rows.Num(); v++)
		{
			Rows[v].Columns.Empty();
		}
		Rows.Empty();
	}
	//default properties
	FBlocksGrid()
	{

	}
};

/** Class used to spawn blocks and manage score */
UCLASS(minimalapi)
class ATRIVARDBlockGrid : public AActor
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** Text component for the score */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* ScoreText;

	/** StaticMesh component for mouse release falling not on blocks */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PlaneMesh;

public:
	ATRIVARDBlockGrid(const FObjectInitializer& ObjectInitializer);

	/** How many blocks have been clicked */
	int32 Score;

	/** Number of blocks along each side of grid */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	int32 Size;

	/** Spacing of blocks */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	float BlockSpacing;

	//create array of objects
	UPROPERTY()
	FBlocksGrid LalaBlocksGrid;

	//Active block count
	int count;

	//Active blocks array
	UPROPERTY()
	TArray<ATRIVARDBlock*> ActiveBlocks;

	//If true then you can hover
	UPROPERTY()
	bool clickStart;

	//Set to start hovering blocks
	UFUNCTION()
	void clickOn();

	//Set to end hovering blocks
	UFUNCTION()
	void clickOff(UPrimitiveComponent* ClickedComp);

	// Begin AActor interface
	virtual void BeginPlay() override;
	// End AActor interface

	/** Handle the block being clicked */
	void AddScore();

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns ScoreText subobject **/
	FORCEINLINE class UTextRenderComponent* GetScoreText() const { return ScoreText; }
};



