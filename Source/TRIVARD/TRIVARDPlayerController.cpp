// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TRIVARD.h"
#include "TRIVARDPlayerController.h"

ATRIVARDPlayerController::ATRIVARDPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
