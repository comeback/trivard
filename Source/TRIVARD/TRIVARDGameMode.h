// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TRIVARDGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ATRIVARDGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATRIVARDGameMode(const FObjectInitializer& ObjectInitializer);
};



