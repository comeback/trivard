// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Actor.h"
#include "TRIVARDBlock.generated.h"

//Enum for block types
namespace EBlockType {
	enum Type{
		Sword,
		Heart,
		Monster		
	};
}

/** A block that can be clicked */
UCLASS(minimalapi)
class ATRIVARDBlock : public AActor
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable plane with image */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PlaneMesh;
	
	//Delete it if you want mesh interactions
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* InteractionCapsule;

public:

	ATRIVARDBlock(const FObjectInitializer& ObjectInitializer);

	/** Are we currently active? */
	bool bIsActive;

	/** Pointers to materials used on blocks */
	UPROPERTY()
	class UMaterialInstance* SwordInactive;
	UPROPERTY()
	class UMaterialInstance* SwordActive;

	UPROPERTY()
	class UMaterialInstance* HeartInactive;
	UPROPERTY()
	class UMaterialInstance* HeartActive;

	UPROPERTY()
	class UMaterialInstance* MonsterInactive;
	UPROPERTY()
	class UMaterialInstance* MonsterActive;

	//Block type
	EBlockType::Type BlockType;

	/** Grid that owns block */
	UPROPERTY()
	class ATRIVARDBlockGrid* OwningGrid;

	/** Handle the block being clicked */
	UFUNCTION()
	void BlockClicked(UPrimitiveComponent* ClickedComp);

	//Handle the block being hovered
	UFUNCTION()
	void BlockHover(UPrimitiveComponent* HoveredComp);

	//Making block active and painting
	UFUNCTION()
	void MakeActive();

	//Making block inactive and painting
	UFUNCTION()
	void MakeInactive();

	//Hande mouse release
	UFUNCTION()
	void clickOff(UPrimitiveComponent* ClickedComp);

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return PlaneMesh; }
};



