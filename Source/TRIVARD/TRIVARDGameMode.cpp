// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TRIVARD.h"
#include "TRIVARDGameMode.h"
#include "TRIVARDPlayerController.h"

ATRIVARDGameMode::ATRIVARDGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// no pawn by default
	DefaultPawnClass = NULL;
	// use our own player controller class
	PlayerControllerClass = ATRIVARDPlayerController::StaticClass();
}
