// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TRIVARD.h"
#include "TRIVARDBlockGrid.h"
#include "TRIVARDBlock.h"
#include "Components/TextRenderComponent.h"


// Structure to hold one-time initialization
struct FConstructorStatics
{
	ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
	FConstructorStatics()
		: PlaneMesh(TEXT("/Game/StarterContent/Shapes/Shape_Plane.Shape_Plane"))
	{
	}
};
static FConstructorStatics ConstructorStatics;


ATRIVARDBlockGrid::ATRIVARDBlockGrid(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	//Create static mesh component
	PlaneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh0"));
	PlaneMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	PlaneMesh->SetRelativeScale3D(FVector(50.f, 50.f, 50.f));
	PlaneMesh->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	PlaneMesh->AttachTo(DummyRoot);

	// Create score text component
	ScoreText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ScoreText0"));
	ScoreText->SetRelativeLocation(FVector(200.f,0.f,0.f));
	ScoreText->SetRelativeRotation(FRotator(90.f,0.f,0.f));
	ScoreText->SetText(TEXT("Score: 0"));
	ScoreText->AttachTo(DummyRoot);

	//Make mouse release interaction
	PlaneMesh->OnReleased.AddDynamic(this, &ATRIVARDBlockGrid::clickOff);

	// Set defaults
	Size = 4;
	BlockSpacing = 251.f;
}


void ATRIVARDBlockGrid::BeginPlay()
{
	Super::BeginPlay();

	// Number of blocks
	const int32 NumBlocks = Size * Size;

	//INIT
	LalaBlocksGrid.Clear();
	LalaBlocksGrid.AddUninitialized(Size, Size);

	// Loop to spawn each block
	for (int32 BlockRow = 0; BlockRow < Size; BlockRow++)
	{
		for (int32 BlockCol = 0; BlockCol < Size; BlockCol++)
		{
			const float XOffset = (BlockRow)* BlockSpacing; // Divide by dimension
			const float YOffset = (BlockCol)* BlockSpacing; // Modulo gives remainder

			// Make postion vector, offset from Grid location
			const FVector BlockLocation = FVector(XOffset, YOffset, 0.f) + GetActorLocation();

			//Make Array
			LalaBlocksGrid.Rows[BlockRow].Columns[BlockCol] = GetWorld()->SpawnActor<ATRIVARDBlock>(BlockLocation, FRotator(0, 0, 0));

			// Tell the block about its owner
			if (LalaBlocksGrid.Rows[BlockRow].Columns[BlockCol] != NULL)
			{
				LalaBlocksGrid.Rows[BlockRow].Columns[BlockCol]->OwningGrid = this;
			}

		}
	}
}

void ATRIVARDBlockGrid::AddScore()
{
	//Do it only if score
	if (count > 2){
		// Increment score
		Score += count * count;

		//Destroy all blocks that were used. Set their pointers to NULL
		for (int row = 0; row < Size; row++){
			for (int column = 0; column < Size; column++){
				if (LalaBlocksGrid.Rows[row].Columns[column]){
					if (LalaBlocksGrid.Rows[row].Columns[column]->bIsActive){
						LalaBlocksGrid.Rows[row].Columns[column]->Destroy();
						LalaBlocksGrid.Rows[row].Columns[column] = NULL;
					}
				}
			}
		}

		// Update text
		FString ScoreString = FString::Printf(TEXT("Score: %d"), Score);
		ScoreText->SetText(ScoreString);

		//Clear array of active blocks
		for (int i = 0; i < count; i++){
			ActiveBlocks[i] = NULL;
		}
		
	}
	else {
		//If nothing is destroyed � deactivate
		for (int i = 0; i < count; i++){
			ActiveBlocks[i]->MakeInactive();
		}
	}
	//No blocks are active
	count = 0;
}
void ATRIVARDBlockGrid::clickOn(){
	clickStart = true;
	count = 1;
}

void ATRIVARDBlockGrid::clickOff(UPrimitiveComponent* ClickedComp){
	clickStart = false;
	AddScore();
}