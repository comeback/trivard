// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TRIVARD.h"
#include "TRIVARDBlock.h"
#include "TRIVARDBlockGrid.h"

ATRIVARDBlock::ATRIVARDBlock(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> SwordInactive;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> HeartInactive;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> MonsterInactive;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> SwordActive;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> HeartActive;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> MonsterActive;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/StarterContent/Shapes/Shape_Plane.Shape_Plane"))
			, SwordInactive(TEXT("/Engine/TemplateResources/MI_Template_BaseBlue.MI_Template_BaseBlue"))
			, HeartInactive(TEXT("/Engine/TemplateResources/MI_Template_BaseOrange.MI_Template_BaseOrange"))
			, MonsterInactive(TEXT("/Engine/TemplateResources/MI_Template_BaseGray.MI_Template_BaseGray"))
			, SwordActive(TEXT("/Engine/TemplateResources/MI_Template_BaseBlue_Metal.MI_Template_BaseBlue_Metal"))
			, HeartActive(TEXT("/Engine/TemplateResources/MI_Template_BaseOrange_Metal.MI_Template_BaseOrange_Metal"))
			, MonsterActive(TEXT("/Engine/TemplateResources/MI_Template_BaseGray_Metal.MI_Template_BaseGray_Metal"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	//Set block type to random value
	switch (rand() % 3){
	case 0:
		BlockType = EBlockType::Heart;
		break;
	case 1:
		BlockType = EBlockType::Sword;
		break;
	case 2:
		BlockType = EBlockType::Monster;
		break;
	}

	// Create static mesh component
	PlaneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh0"));
	PlaneMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	PlaneMesh->SetRelativeScale3D(FVector(2.56f, 2.56f, 2.56f));
	PlaneMesh->SetRelativeLocation(FVector(0.f,0.f,0.1f));
	switch (BlockType)
	{
	case EBlockType::Heart:
		PlaneMesh->SetMaterial(0, ConstructorStatics.HeartInactive.Get());
		break;
	case EBlockType::Sword:
		PlaneMesh->SetMaterial(0, ConstructorStatics.SwordInactive.Get());
		break;
	case EBlockType::Monster:
		PlaneMesh->SetMaterial(0, ConstructorStatics.MonsterInactive.Get());
		break;
	}
	
	PlaneMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	PlaneMesh->AttachTo(DummyRoot);

	//Create Collision Capsule
	InteractionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("InteractionCapsule0"));
	InteractionCapsule->SetRelativeScale3D(FVector(5.483724f, 5.721239f, 4.0f));
	InteractionCapsule->SetRelativeLocation(FVector(0.f, 0.f, 7.052505f));
	InteractionCapsule->SetCollisionResponseToAllChannels(ECR_Block);
	InteractionCapsule->AttachTo(DummyRoot);

	//This is type, where collision Capsule used for player interactions
	InteractionCapsule->OnClicked.AddDynamic(this, &ATRIVARDBlock::BlockClicked);
	InteractionCapsule->OnReleased.AddDynamic(this, &ATRIVARDBlock::clickOff);
	InteractionCapsule->OnBeginCursorOver.AddDynamic(this, &ATRIVARDBlock::BlockHover);

	//And this is type of game where mesh used to check player interactions
	//PlaneMesh->OnClicked.AddDynamic(this, &ATRIVARDBlock::BlockClicked);
	//PlaneMesh->OnReleased.AddDynamic(this, &ATRIVARDBlock::clickOff);
	//PlaneMesh->OnBeginCursorOver.AddDynamic(this, &ATRIVARDBlock::BlockHover);

	

	// Save a pointer to the orange and blue materials
	HeartInactive = ConstructorStatics.HeartInactive.Get();
	SwordInactive = ConstructorStatics.SwordInactive.Get();
	MonsterInactive = ConstructorStatics.MonsterInactive.Get();
}

void ATRIVARDBlock::BlockClicked(UPrimitiveComponent* ClickedComp)
{
	// Check we are not already active
	if(!bIsActive)
	{
		//Make active, start hovering
		MakeActive();
		OwningGrid->clickOn();
		int where = OwningGrid->count - 1;
		//Add it to active blocks array
		OwningGrid->ActiveBlocks.Insert(this,where);
	}
	
}

void ATRIVARDBlock::BlockHover(UPrimitiveComponent* HoveredComp){
//	UE_LOG(LogTemp, Warning, TEXT("HOVER"));

	//Only do it if mouse button is pressed
	if (OwningGrid->clickStart){
		if (!bIsActive) {
			//Check whether blocks is of the same type
			if (OwningGrid->ActiveBlocks[0]->BlockType == this->BlockType){
				//Make active
				MakeActive();

				//Increase active blocks count and push this block to array of active blocks
				OwningGrid->count++;
				int where = OwningGrid->count - 1;
				OwningGrid->ActiveBlocks.Insert(this, where);
			}
		}
		else {
			int where = OwningGrid->count - 2;
			//Check whether this block is same as last one
			if (where > 0){
				if (this == OwningGrid->ActiveBlocks[where]){
					//Deactivate previous
					where++;
					OwningGrid->ActiveBlocks[where]->MakeInactive();

					//Decrease the count so the array will be filled starting from this position
					//Which leads to overwriting this position if more are selected anyway
					OwningGrid->count--;
				}
			}
		}
	}
}

void ATRIVARDBlock::MakeActive(){
	//Make active
	bIsActive = true;
	// Change material
	switch (BlockType)
	{
	case EBlockType::Heart:
		PlaneMesh->SetMaterial(0, HeartActive);
		break;
	case EBlockType::Sword:
		PlaneMesh->SetMaterial(0, SwordActive);
		break;
	case EBlockType::Monster:
		PlaneMesh->SetMaterial(0, MonsterActive);
		break;
	}
	
}

void ATRIVARDBlock::MakeInactive(){
	//Make active
	bIsActive = false;
	// Change material
	switch (BlockType)
	{
	case EBlockType::Heart:
		PlaneMesh->SetMaterial(0, HeartInactive);
		break;
	case EBlockType::Sword:
		PlaneMesh->SetMaterial(0, SwordInactive);
		break;
	case EBlockType::Monster:
		PlaneMesh->SetMaterial(0, MonsterInactive);
		break;
	}
}

void ATRIVARDBlock::clickOff(UPrimitiveComponent* ClickedComp){
	OwningGrid->clickOff(ClickedComp);
}
