// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "TRIVARDPlayerController.generated.h"

/** PlayerController class used to enable cursor */
UCLASS()
class ATRIVARDPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATRIVARDPlayerController(const FObjectInitializer& ObjectInitializer);
};


